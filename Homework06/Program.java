public class Program {
    public static int getIndex(int[] array, int element) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == element) {
                return i;
            }
        }
        return -1;
    }

    public static void dropNulles(int[] array, int swapNumber) {
        for(int i = array.length -1; i >= 0; i--) {
            if(array[i] == 0) {
                array[i] = array[array.length - 1 - swapNumber];
                array[array.length - 1 - swapNumber] = 0;
                swapNumber++;
            }
        }

        for(int i : array) {
            System.out.print(i + ", ");
        }
    }

    public static void main(String[] args) {
        int[] array = new int[]{4, 0, 0, 0, 0, 8};
        int element = 3;
        int swapNumber = 0;
        System.out.println(getIndex(array, element));
        dropNulles(array, swapNumber);
    }
}
