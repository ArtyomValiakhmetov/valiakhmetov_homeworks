import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String userString = scanner.nextLine();
        Set<Map.Entry<String, Integer>> countedWords = getCountOfWord(userString).entrySet();
        for (Map.Entry<String, Integer> words: countedWords) {
            System.out.println(words.getKey() + " - " + words.getValue());
        }
    }

    public static Map<String, Integer> getCountOfWord(String str) {
        Map<String, Integer> numWords = new HashMap<>();
        String[] words = str.split(" ");
        for (String word : words) {
            if (numWords.containsKey(word)) {
                int count = numWords.get(word) + 1;
                numWords.put(word, count);
            } else {
                numWords.put(word, 1);
            }
        }

        return numWords;
    }
}
