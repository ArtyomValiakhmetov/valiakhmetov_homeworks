import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int minDigit = 10; // минимальная цифра
        // пока не встретили -1
        while (a != -1) {
            while (a != 0) {
                int nowDigit = a % 10;
                if (nowDigit < minDigit) {
                    minDigit = nowDigit;
                }
                a = a / 10;
            }
            // на каждом шаге цикла считываем новое число из консоли
            a = scanner.nextInt();
        }
        // выводим результат
        System.out.println("Ответ: "  + minDigit);
    }
}
