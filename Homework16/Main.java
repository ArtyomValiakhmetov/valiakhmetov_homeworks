public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> digits = new ArrayList<>();
        digits.add(33);
        digits.add(15);
        digits.add(11);
        digits.add(89);
        digits.add(17);
        digits.add(21);

        digits.removeAt(5);
        digits.removeAt(3);
        digits.removeAt(1);

        for (int i = 0; i < digits.getSize(); i++) {
            System.out.println(digits.get(i));
        }
        System.out.println(digits.get(8));

        System.out.println();

        LinkedList<Integer> list = new LinkedList<>();
        list.add(34);
        list.add(120);
        list.add(-10);

        list.addToBegin(77);
        list.addToBegin(88);
        list.addToBegin(99);

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println(list.get(8));
    }

}

