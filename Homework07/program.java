import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * На вход подается последовательность чисел, оканчивающихся на -1.
 * Необходимо вывести число, которе присутствует в последовательности
 * минимальное количество раз.
 * Гарантируется:
 * Все числа в диапазоне от -100 до 100.
 * Числа встречаются не более 2 147 483 647-раз каждое.
 * Сложность алгоритма - O(n)
 */

public class Program1 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        while (true){
            try {
                int a = scanner.nextInt();
                // массив для хранения количества повторений каждого числа
                // 2 147 483 647 - максимальное значение для int
                int[] elements = new int[201]; // 201 число, в сумме в промежутке от -100 до 100
                while (a != -1) {
                    // увеличиваем количество повторений на единицу
                    elements[a + 100]++;
                    a = scanner.nextInt();
                }

                int maxCount = 2_147_483_647;
                int minIndex = 0;
                for(int i = 0; i < elements.length; i ++) {
                    if(elements[i] < maxCount && elements[i] != 0){
                        maxCount = elements[i];
                        minIndex = i;
                    }
                }
                // реверсивной операцией вычисляем само число
                System.out.println("Минимальное количество раз присутствует число: " + (minIndex - 100));
                // если пользователь ошибся
            } catch (InputMismatchException exception){
                System.out.println("Введите число от -100 до 100");
            }
            finally {
                scanner.nextLine();
            }
        }
    }
}