public class Car {
    private String number;
    private String model;
    private String color;
    private int run;
    private int price;

    public Car(String number, String model, String color, int run, int price) {
        this.number = number;
        this.model = model;
        this.color = color;
        this.run = run;
        this.price = price;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getRun() {
        return run;
    }

    public void setRun(int run) {
        this.run = run;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public static int compare (Car car1, Car car2) {
        if(car1.getPrice() > car2.getPrice())
            return 1;
        return -1;
    }

}
