import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        try(BufferedReader reader = new BufferedReader(new FileReader("input.txt"))) {
            List<Car> cars = reader.lines()
                    .map(line -> line.split("\\|"))
                    .map(line -> new Car(line[0], line[1], line[2], Integer.parseInt(line[3]),Integer.parseInt(line[4])))
                    .collect(Collectors.toList());

            System.out.println("Номера машин черного цвета или без пробега:");
            cars.stream()
                    .filter(car -> car.getColor().equals("Black") || car.getRun() == 0)
                    .forEach(car -> System.out.print(car.getNumber() + " "));
            System.out.println();

            System.out.println("Количество уникальльных моделей: " +
                    cars.stream()
                            .filter(car -> car.getPrice() >= 700000 && car.getPrice() <= 800000)
                            .map(Car::getModel)
                            .distinct()
                            .count());

            System.out.print("Цвет автомобиля с минимальной стоимостью: ");
            cars.stream()
                    .min(Car::compare)
                    .map(Car::getColor)
                    .ifPresent(System.out::println);

            System.out.print("Средняя стоимость K5: ");
            cars.stream()
                    .filter(car -> car.getModel().equals("K5"))
                    .mapToInt(Car::getPrice)
                    .average()
                    .stream()
                    .mapToInt(num -> (int) num)
                    .forEach(System.out::println);

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
