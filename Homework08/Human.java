public class Human {

    private String name;
    private double weight;

    public Human(String name, double weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * Вывод в удобном виде
     */
    @Override
    public String toString() {
        return "Human{" + "name='" + name + '\'' + ", weight=" + weight + '}';
    }

}
