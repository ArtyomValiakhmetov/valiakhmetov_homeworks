public class HumanSort {
    public static void sortHumansByWeight(Human[] humans) {
        for (int i = 0; i < humans.length; i++) {
            double minWeight = humans[i].getWeight();
            int minWeightIndex = i;
            for (int j = i + 1; j < humans.length; j++) {
                if(humans[j].getWeight() < minWeight) {
                    minWeight = humans[j].getWeight();
                    minWeightIndex = j;
                }
            }
            // Ставим в начало человека с минимальным весом
            Human tmp = humans[i];
            humans[i] = humans[minWeightIndex];
            humans[minWeightIndex] = tmp;
        }
    }
}
