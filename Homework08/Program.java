import java.util.Arrays;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Human[] humans = new Human[10];
        for(int index = 0; index < humans.length; index++) {
            String name = scanner.next();
            while (!scanner.hasNextDouble()){
                System.out.println("Введите вещественное число в нужном формате");
                scanner.nextLine();
            }
            double weight = scanner.nextDouble();
            if(weight > 0) {
                weight = 0;
            }
            else {
                System.out.println("Вес человека должен быть положительным");
            }
            humans[index] = new Human(name, weight);
        }
        System.out.println("Обычный вид:  ");
        System.out.println(Arrays.toString(humans));
        System.out.println();
        System.out.println("Сортировочный: ");
        HumanSort.sortHumansByWeight(humans);
        System.out.println(Arrays.toString(humans));
    }
}
