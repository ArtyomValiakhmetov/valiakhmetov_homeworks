public class SumThread extends Thread {
    private int from;
    private int to;
    private int sum;
    private int[] array;

    public int getSum() {
        return sum;
    }

    public SumThread(int from, int to, int[] array) {
        this.from = from;
        this.to = to;
        this.array = array;
    }

    @Override
    public void run() {
        for (int i = from - 1; i < to; i++) {
            sum += array[i];
        }
    }
}
