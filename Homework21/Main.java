import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static int[] array;
    public static int[] sums;

    public static void main(String[] args) {
        // чтобы посмотреть наглядно что потоки работают параллельно,
        // нужно снять "//" с последней строчки в методе run класса SumThread
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите количество чисел: ");
        int numbersCount = scanner.nextInt();
        System.out.print("Введите количество потоков: ");
        int threadsCount = scanner.nextInt();

        array = new int[numbersCount];
        sums = new int[threadsCount];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }

        int realSum = 0;

        for (int number : array) {
            realSum += number;
        }

        System.out.println(realSum);

        List<SumThread> sumThreadList = new ArrayList<>();
        for (int thread = 1; thread <= threadsCount; thread++) {
            int from = numbersCount / threadsCount * (thread - 1) + 1;
            int to = numbersCount / threadsCount * thread;
            if (thread == threadsCount) {
                to = to + numbersCount % threadsCount;
            }
            sumThreadList.add(new SumThread(from, to, array));
        }

        for (int thread = 1; thread <= threadsCount; thread++) {
            sumThreadList.get(thread - 1).start();
        }

        try {
            for (int thread = 1; thread <= threadsCount; thread++) {
                sumThreadList.get(thread - 1).join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int thread = 1; thread <= threadsCount; thread++) {
            sums[thread - 1] = sumThreadList.get(thread - 1).getSum();
        }

        int byThreadSum = 0;

        for (int i = 0; i < threadsCount; i++) {
            byThreadSum += sums[i];
        }

        System.out.println(byThreadSum);

        if (realSum == byThreadSum) {
            System.out.println("Числа равны.");
        }
    }

}
