public class Program {
    public static void main(String[] args) {
        Traffic[] goObjects = new Traffic[3];
        Circle circle1 = new Circle(5, 10);
        circle1.buildCircle(4);
        goObjects[0] = circle1;

        Square square = new Square(-3, -4);
        square.buildSquare(12);
        goObjects[1] = square;

        Circle circle2 = new Circle(0, 0);
        circle2.buildCircle(1.5);
        goObjects[2] = circle2;

        for (Traffic moveObject : goObjects) {
            moveObject.goToPoint(5, 10);
        }

}
