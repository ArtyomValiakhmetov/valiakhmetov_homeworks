public class Square extends  Rectangle implements Traffic {
    /**
     * Конструктор квадрата
     * @param x координата x
     * @param y координата y
     */
    public Square(double x, double y) {
        super(x, y);
    }

    public void buildSquare(double side) {
        buildRectangle(side, side);
    }

    /**
     *  Перемещение квадрата в точку с координатами икс и игрик
     */
    @Override
    public void goToPoint(double x, double y) {
        System.out.println("Начальные координаты");
        showSquareCoordinates();
        this.x += (x - this.y);
        this.y += (y - this.y);
        System.out.println("Координаты после смещения");
        showSquareCoordinates();
    }

    public void showSquareCoordinates() {
        System.out.println("x центр: " + this.x + " y центр: " + this.y + "\n" +
                "x1: " + (this.x - (width/2)) + " y1: " + (this.y - (width/2)) + "\n" + "x2: " + (this.x + (width/2)) + " y2: " + (this.y - (width/2)) + "\n" + "x3: " + (this.x + (width/2)) + " y3: " + (this.y + (width/2)) + "\n" + "x4: " + (this.x - (width/2)) + " y4: " + (this.y + (width/2)) + "\n");

    }
}
