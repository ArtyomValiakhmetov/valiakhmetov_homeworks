public class Ellipse extends Figure {

    protected double radius1;
    protected double radius2;

    /**
     * конструктор
     * @param x координата x
     * @param y координата y
     */
    public Ellipse(double x, double y) {
        super(x, y);
    }

    public void buildEllipse(double radius1, double radius2) {
        this.radius1 = radius1;
        this.radius2 = radius2;
    }
}
