public class Rectangle extends Figure {

    protected double width;
    protected double length;

    /**
     * Конструктор прямоугольника
     * @param x координата x центра прямоугольника
     * @param y координата y центра прямоугольника
     */
    public Rectangle(double x, double y) {
        super(x, y);
    }

    public void buildRectangle(double width, double length){
        this.width = width;
        this.length = length;
    }
}
