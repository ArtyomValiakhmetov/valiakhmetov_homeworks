public class Circle extends Ellipse implements Traffic {
    /**
     * Конструктор
     * @param x координата x
     * @param y координата y
     */
    public Circle(double x, double y) {
        super(x, y);
    }

    public void buildCircle(double radius) {
        buildEllipse(radius, radius);
    }

    /**
     *  Перемещение круга в точку с координатами икс и игрик
     */
    @Override
    public void goToPoint(double x, double y) {
        System.out.println("Начальные координаты");
        showСircleCoordinates();
        this.x += (x - this.y);
        this.y += (y - this.y);
        System.out.println("Координаты после смещения");
        showСircleCoordinates();
    }

    public void showСircleCoordinates() {
        System.out.println(
                "x центр: "+ this.x + " y центр: " +this.y + "\n" + "x1: "+ (this.x + radius1) + " y1: "+ this.y + "\n" + "x2: "+ this.x + " y2: "+ (this.y + radius1) + "\n" + "x3: "+ (this.x - radius1) + " y3: "+ this.y + "\n" + "x4: "+ this.x + " y4: "+ (this.y - radius1) + "\n");

    }
}
