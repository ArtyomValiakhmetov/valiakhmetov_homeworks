public class Program {
    public static void main(String[] args) {
        Figure[] figures = new Figure[5];
        figures[0] = new Figure(0, 0);

        Ellipse ellipse = new Ellipse(3, 4);
        ellipse.buildEllipse(12, 14);
        figures[1] = ellipse;

        Circle circle = new Circle(10, 12);
        circle.buildCircle(20);
        figures[2] = circle;

        Rectangle rectangle = new Rectangle(-2, 4);
        rectangle.buildRectangle(10, 12);
        figures[3] = rectangle;

        Square square = new Square(2, 2);
        square.buildSquare(98);
        figures[4] = square;

        for(Figure figure: figures) {
            System.out.println(figure.getPerimeter());
        }
    }
}
