public class Circle extends Ellipse {
    /**
     * Конструктор
     * @param x координата x
     * @param y координата y
     */
    public Circle(double x, double y) {
        super(x, y);
    }

    public void buildCircle(double radius) {
        buildEllipse(radius, radius);
    }
}
