public class Square extends  Rectangle {
    /**
     * Конструктор квадрата
     * @param x координата x
     * @param y координата y
     */
    public Square(double x, double y) {
        super(x, y);
    }

    public void buildSquare(double side) {
        buildRectangle(side, side);
    }

}
