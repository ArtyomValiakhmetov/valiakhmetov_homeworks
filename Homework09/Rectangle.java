public class Rectangle extends Figure {
    private double width;
    private double length;

    /**
     * Конструктор прямоугольника
     * @param x координата x центра прямоугольника
     * @param y координата y центра прямоугольника
     */
    public Rectangle(double x, double y) {
        super(x, y);
    }

    /**
     * Установка длины и ширины
     * @param width ширина прямоугольника
     * @param length длина прямоугольника
     */
    public void buildRectangle(double width, double length){
        this.width = width;
        this.length = length;
    }

    @Override
    public double getPerimeter() {
        return width * 2 + length * 2;
    }
}
