public class Figure {
    protected double x;
    protected double y;

    /**
     * Конструктор фигуры
     * @param x координата
     * @param y координата
     */
    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getPerimeter() {
        return 0.0;
    }
}
