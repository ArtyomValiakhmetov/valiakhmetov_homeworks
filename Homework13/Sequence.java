import java.util.ArrayList;
import java.util.List;

public class Sequence {
    /**
     * @return filtered int[]Array
     *
     * @author ArtyomValiakhmetov <xhuman80@gmail.com>
     */
    public static int[] filter(int[] array, ByCondition condition) {
        List<Integer> result = new ArrayList<>();
        for(int number : array) {
            if(condition.isOk(number)){
                result.add(number);
            }
        }
        return result.stream().mapToInt(Integer::intValue).toArray();
    }
}
