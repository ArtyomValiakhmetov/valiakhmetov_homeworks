import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = {11, 12, -45, 10, 5};
        // проверка на четность элемента
        // массив, который содержит только четные элементы массива array
        int[] evenNumbers = Sequence.filter(array, number -> number % 2 == 0);

        // проверка, является ли сумма цифр элемента четным числом
        // массив, который содержит только те числа, у которых сумма цифр является четной
        int[] evenSums = Sequence.filter(array, number -> {
            int digitsSum = 0;
            while (number != 0) {
                digitsSum += Math.abs(number % 10);
                number /= 10;
            }
            return digitsSum % 2 == 0;
        });

        System.out.println(Arrays.toString(evenNumbers));
        System.out.println(Arrays.toString(evenSums));

    }
}
