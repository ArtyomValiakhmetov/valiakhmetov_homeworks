import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepository {

    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void save(User user) {
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, true))) {
            bufferedWriter.write(user.getName() + "|" + user.getAge() + "|" + user.isWorker() );
            bufferedWriter.newLine();
            // сброс буфера
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<User> findByAge(int age) {
        List<User> users = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int ageInFile = getAge(parts[1]);
                if(ageInFile == age) {
                    users.add(new User(parts[0], ageInFile, getIsWorker(parts[2])));
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException | IndexOutOfBoundsException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    @Override
    public List<User> findByIsWorkerIsTrue() {
        List<User> users = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                if(getIsWorker(parts[2])) {
                    users.add(new User(parts[0], getAge(parts[1]), true));
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException | IndexOutOfBoundsException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    private boolean getIsWorker(String isWorkerInStringFormat) {
        boolean isWorkerParsed = false;
        try {
            isWorkerParsed = Boolean.parseBoolean(isWorkerInStringFormat);
        } catch (NumberFormatException ignore) {}
        return isWorkerParsed;
    }

    private int getAge(String ageInStringFormat) {
        int ageParsed = 0;
        try {
            ageParsed = Integer.parseInt(ageInStringFormat);
        } catch (NumberFormatException ignore) {}
        return ageParsed;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                users.add(new User(parts[0], getAge(parts[1]), getIsWorker(parts[2])));

                line = bufferedReader.readLine();
            }
        } catch (IOException | IndexOutOfBoundsException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }
}
