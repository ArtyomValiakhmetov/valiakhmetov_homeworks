import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        usersRepository.save(new User("Николай", 15, true));
        usersRepository.save(new User("Илья", 35, false));
        usersRepository.save(new User("Петр", 15, true));

        System.out.println("Все пользователи: ");
        printUsers(usersRepository.findAll());
        System.out.println();

        int ageToSearch = 20;
        System.out.println("Все пользователи с возврастом " + ageToSearch);
        printUsers(usersRepository.findByAge(ageToSearch));
        System.out.println();

        System.out.println("Все работающие пользователи: ");
        printUsers(usersRepository.findByIsWorkerIsTrue());
    }

    public static void printUsers(List<User> usersToPrint) {
        usersToPrint.forEach(user -> System.out.println(user.getName() + " " + user.getAge() + " " + user.isWorker()));
    }
}
